<html>
<head>
    <title>Welcome to IntelliInvest</title>
    <link href="webjars/bootstrap/3.3.6/css/bootstrap.min.css"
        rel="stylesheet">
    <link href="css/custom.css"
        rel="stylesheet">
</head>
<body>
    <div class="container">
        <table class="table table-striped">
            <caption>Welcome to intelliinvest</caption>
        </table> 
        <form>
   <div class="col-sm-8">
  <div class="form-group">
    <label for="contactNumber">Contact Number *</label>
    <input type="text" class="form-control" id="contactNumber" aria-describedby="contactNumberHelp"
            placeholder="Enter multiple contact number with semicolon...">
    <small id="contactNumberHelp" class="form-text text-muted">contact number is required.</small>
    </div>
     </div>
     <div class="col-sm-4">
     <div class="form-group" >
        <label for="xlsFile">Choose xls file</label>
        <input type="file" class="form-control-file" id="xlsFile" >
      </div>
      </div>
 
   <div class="col-sm-12">
  <div class="form-group">
    <label for="messageText">Message *</label>
    <textarea class="form-control rounded-0" id="messageText" aria-describedby="messageTextHelp" placeholder="Enter message..." rows="10"></textarea>
   <small id="messageTextHelp" class="form-text text-muted">Message is required.</small>
  </div>
  </div>
  <div class="col-sm-12">
  <button type="submit" class="btn btn-success" onclick="sendMessage()">Send Message</button>
  <button type="reset" class="btn btn-warning" onclick="resetMessage()">Reset Message</button>
  </div>
</form>
        <script src="webjars/jquery/1.9.1/jquery.min.js"></script>
        <script src="webjars/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        <script src="js/custom.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.13.5/xlsx.full.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.13.5/jszip.js"></script>
    </div>
</body>
</html>