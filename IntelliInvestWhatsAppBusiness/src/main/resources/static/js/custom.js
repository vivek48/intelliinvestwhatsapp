//alert("I'm active");


 document.getElementById('xlsFile')
            .addEventListener('change', function() {
            Upload()
        })



function Upload() {
        //Reference the FileUpload element.
       var fileUpload = document.getElementById("xlsFile");
 
        //Validate whether File is valid Excel file.
        var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.xls|.xlsx)$/;
        if (regex.test(fileUpload.value.toLowerCase())) {
            if (typeof (FileReader) != "undefined") {
                var reader = new FileReader();
 
                //For Browsers other than IE.
                if (reader.readAsBinaryString) {
                    reader.onload = function (e) {
                        ProcessExcel(e.target.result);
                    };
                    reader.readAsBinaryString(fileUpload.files[0]);
                } else {
                    //For IE Browser.
                    reader.onload = function (e) {
                        var data = "";
                        var bytes = new Uint8Array(e.target.result);
                        for (var i = 0; i < bytes.byteLength; i++) {
                            data += String.fromCharCode(bytes[i]);
                        }
                        ProcessExcel(data);
                    };
                    reader.readAsArrayBuffer(fileUpload.files[0]);
                }
            } else {
                alert("This browser does not support HTML5.");
            }
        } else {
            alert("Please upload a valid Excel file.");
        }
    };
    
    function ProcessExcel(data) {
        //Read the Excel File data.
        var workbook = XLSX.read(data, {
            type: 'binary'
        });
 
        //Fetch the name of First Sheet.
        var firstSheet = workbook.SheetNames[0];
 
        //Read all rows from First Sheet into an JSON array.
        var excelRows = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[firstSheet]);
        var ContactNumber="";
       for(var i=0;i<excelRows.length;i++)
    	   {
    	   if(ContactNumber+excelRows[i].ContactNumber!=undefined&&ContactNumber+excelRows[i].ContactNumber!=null&&ContactNumber+excelRows[i].ContactNumber!=""){
    	       ContactNumber=ContactNumber+excelRows[i].ContactNumber.trim()+ (i!=excelRows.length-1?",":"");
    	   }
    	   }
       
       $("#contactNumber").val(ContactNumber);
    }




function sendMessage() {
		var contactNumber=document.getElementById("contactNumber").value;
	var message=document.getElementById("messageText").value;   
	var data={};
	if(contactNumber!=null&&contactNumber!=undefined&&contactNumber!="")
		{
		data.contactNumberList=contactNumber;
		}else{
			alert("Contact number required.");
			return false
		}
	if(message!=null&&message!=undefined&&message!="")
	{
	data.message=message;
	}else{
		alert("message required.");
		return false
	}
	
	// PREPARE FORM DATA
	    //	contactNumberList:contactNumber,message:message,};
	    	// DO POST
	    	$.ajax({
				type : "POST",
				contentType : "application/json",
				url :"http://localhost:8093/intelliinvest/api/sendWhatsappMessage/toUsers",
				data : JSON.stringify(data),
				dataType : 'json',
				success : function(result) {
					if(result.statusCode == 200){
						resetMessage()
						alert(result.statusMsg);
						 window.location = "http://localhost:8093/intelliinvest/api/welcome";
					} else {
						alert(result.data);
					}
				},
				error : function(e) {
					alert("Error!")
					console.log("ERROR: ", e);
				}
			});
	
}

function resetMessage() {
	$("#contactNumber").val("");
	$("#messageText").val("");
	$("#xlsFile").val("");
}