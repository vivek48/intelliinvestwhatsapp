package com.intelliinvest.controller;
import java.util.concurrent.TimeUnit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.CacheControl;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.intelliinvest.comman.Helper;
import com.intelliinvest.comman.IntelliinvestException;
import com.intelliinvest.dto.WhatsappMessageBody;
import com.intelliinvest.response.PostResponse;
import com.intelliinvest.whatsapp.twilio.service.TwilioService;
@RestController
public class WhatsAppEventController extends IntelliInvestBaseController {
	private static final String APPLICATION_JSON = "application/json";

	@Autowired
	private TwilioService twillioService;
	
	@RequestMapping(value = "/sendWhatsappMessage/users", method = RequestMethod.POST, consumes=APPLICATION_JSON,produces=APPLICATION_JSON)  
	public ResponseEntity<PostResponse> sendMessageToUsers(@RequestBody WhatsappMessageBody whatsappMessage)
	{
		String version = "1.0.0";
		 PostResponse postResponse;
		try {
			if (!(Helper.isNotNullAndNonEmpty(String.valueOf(whatsappMessage.getContactNumber())) && Helper.isNotNullAndNonEmpty(whatsappMessage.getMessage()))) {
				throw new IntelliinvestException("Invalid input. Please check contact number and message.");
			}
			twillioService.sendMessageToUsers(whatsappMessage.getContactNumber(), whatsappMessage.getMessage());
            postResponse =postResponseObject(200, "Send message successfully","");
			return ResponseEntity.ok().cacheControl(CacheControl.maxAge(30, TimeUnit.DAYS)).eTag(version) // lastModified
					.header("Access-Control-Allow-Origin", "*").body(postResponse);
		}catch(Exception e)
		{
			postResponse = postResponseObject(400, "ERROR", e.getMessage());
			return ResponseEntity.ok().cacheControl(CacheControl.maxAge(30, TimeUnit.DAYS)).eTag(version) // lastModified
					.header("Access-Control-Allow-Origin", "*").body(postResponse);
			
		}
	}


}
