package com.intelliinvest.controller;

import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.CacheControl;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.intelliinvest.comman.Helper;
import com.intelliinvest.comman.IntelliinvestException;
import com.intelliinvest.dto.WhatsappMessageBody;
import com.intelliinvest.response.PostResponse;
import com.intelliinvest.whatsapp.twilio.service.TwilioService;

@Controller
public class SendMessageController extends IntelliInvestBaseController {

	 private static final String APPLICATION_JSON = "application/json";

		@Autowired
		private TwilioService twillioService;
	 @RequestMapping("/welcome")
	    public String loginMessage(){
	        return "welcome";
	    }
		@RequestMapping(value = "/sendWhatsappMessage/toUsers", method = RequestMethod.POST, consumes=APPLICATION_JSON,produces=APPLICATION_JSON)  
		public @ResponseBody ResponseEntity<PostResponse> sendMessageToUsers(@RequestBody WhatsappMessageBody requestMessage)
		{
			String version = "1.0.0";
			 PostResponse postResponse;
			try {
				if (!(Helper.isNotNullAndNonEmpty(requestMessage.getContactNumberList()) && Helper.isNotNullAndNonEmpty(requestMessage.getMessage()))) {
					throw new IntelliinvestException("Invalid input. Please check contact number and message.");
				}
				String[] contactList=requestMessage.getContactNumberList().split(",");
				for(String contact:contactList)
				{
					try{
				         twillioService.sendMessageToUsers(Long.parseLong(contact), requestMessage.getMessage());
					}catch(Exception e)
					{
						System.out.println("Error " + e.getMessage());
					}
				}
				postResponse =postResponseObject(200, "Send message successfully","");
				return ResponseEntity.ok().cacheControl(CacheControl.maxAge(30, TimeUnit.DAYS)).eTag(version) // lastModified
						.header("Access-Control-Allow-Origin", "*").body(postResponse);
			}catch(Exception e)
			{
				postResponse = postResponseObject(400, "ERROR", e.getMessage());
				return ResponseEntity.ok().cacheControl(CacheControl.maxAge(30, TimeUnit.DAYS)).eTag(version) // lastModified
						.header("Access-Control-Allow-Origin", "*").body(postResponse);
				
			}
		}

}
