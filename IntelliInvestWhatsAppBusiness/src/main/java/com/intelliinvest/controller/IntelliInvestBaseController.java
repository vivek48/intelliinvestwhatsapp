package com.intelliinvest.controller;

import com.intelliinvest.response.GetResponse;
import com.intelliinvest.response.PostResponse;

public class IntelliInvestBaseController {
	private PostResponse postResponse;
	private GetResponse getResponse;
	public PostResponse postResponseObject(int statusCode,String statusmsg,Object data)
	{
		postResponse=new PostResponse();
		postResponse.setStatusCode(statusCode);
		postResponse.setStatusMsg(statusmsg);;
		postResponse.setData(data);
		return postResponse;
	}
	public GetResponse getResponseObject(int statusCode,String statusmsg,Object data)
	{
		getResponse=new GetResponse();
		getResponse.setStatusCode(statusCode);
		getResponse.setStatusMsg(statusmsg);
		getResponse.setData(data);
		return getResponse;
	}
}
