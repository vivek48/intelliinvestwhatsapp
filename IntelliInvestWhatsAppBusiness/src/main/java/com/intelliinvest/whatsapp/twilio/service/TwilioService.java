package com.intelliinvest.whatsapp.twilio.service;

import org.springframework.stereotype.Component;

import com.intelliinvest.util.IntelliInvestConstant;
import com.twilio.rest.api.v2010.account.Message;

@Component
public class TwilioService {
	public void sendMessageToUsers(long userPhoneNumber,String body)
	{
		Message.creator( 
                new com.twilio.type.PhoneNumber("whatsapp:+91"+userPhoneNumber), 
                new com.twilio.type.PhoneNumber("whatsapp:"+IntelliInvestConstant.BUSINESS_PHONE_NUMBER),  
                body)      
            .create(); 
	}
	public void sendMediaMessageToUsers(long userPhoneNumber,String body)
	{
		Message.creator( 
                new com.twilio.type.PhoneNumber("whatsapp:+91"+userPhoneNumber), 
                new com.twilio.type.PhoneNumber("whatsapp:"+IntelliInvestConstant.BUSINESS_PHONE_NUMBER),  
                body)      
            .create(); 
	}

}
