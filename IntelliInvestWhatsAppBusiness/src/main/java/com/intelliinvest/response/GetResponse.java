package com.intelliinvest.response;
public class GetResponse {
	private int statusCode;
	private String statusMsg;
	private Object data;
	public GetResponse() {
		super();
		// TODO Auto-generated constructor stub
	}
	public GetResponse(int statusCode, String statusMsg, Object data) {
		super();
		this.statusCode = statusCode;
		this.statusMsg = statusMsg;
		this.data = data;
	}
	public int getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}
	public String getStatusMsg() {
		return statusMsg;
	}
	public void setStatusMsg(String statusMsg) {
		this.statusMsg = statusMsg;
	}
	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data = data;
	}
	
	

}

