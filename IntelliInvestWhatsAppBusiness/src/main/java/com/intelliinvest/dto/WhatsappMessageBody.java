package com.intelliinvest.dto;

import java.util.List;

public class WhatsappMessageBody {
	
	private String message;
	private long contactNumber;
	private String contactNumberList;
	private List<String> urlLink;
	public WhatsappMessageBody() {
		super();
		// TODO Auto-generated constructor stub
	}
	public WhatsappMessageBody(String message, Integer contactNumber) {
		super();
		this.message = message;
		this.contactNumber = contactNumber;
	}
	
	public WhatsappMessageBody(String message, String contactNumberList) {
		super();
		this.message = message;
		this.contactNumberList = contactNumberList;
	}
	
	
	public WhatsappMessageBody(String message, Integer contactNumber, List<String> urlLink) {
		super();
		this.message = message;
		this.contactNumber = contactNumber;
		this.urlLink = urlLink;
	}
	
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<String> getUrlLink() {
		return urlLink;
	}
	public void setUrlLink(List<String> urlLink) {
		this.urlLink = urlLink;
	}
	
	public long getContactNumber() {
		return contactNumber;
	}
	public void setContactNumber(long contactNumber) {
		this.contactNumber = contactNumber;
	}
	public String getContactNumberList() {
		return contactNumberList;
	}
	public void setContactNumberList(String contactNumberList) {
		this.contactNumberList = contactNumberList;
	}
	
	@Override
	public String toString() {
		return "WhatsappMessageBody [message=" + message + ", contactNumber=" + contactNumber + ", contactNumberList="
				+ contactNumberList + ", urlLink=" + urlLink + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (contactNumber ^ (contactNumber >>> 32));
		result = prime * result + ((contactNumberList == null) ? 0 : contactNumberList.hashCode());
		result = prime * result + ((message == null) ? 0 : message.hashCode());
		result = prime * result + ((urlLink == null) ? 0 : urlLink.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		WhatsappMessageBody other = (WhatsappMessageBody) obj;
		if (contactNumber != other.contactNumber)
			return false;
		if (contactNumberList == null) {
			if (other.contactNumberList != null)
				return false;
		} else if (!contactNumberList.equals(other.contactNumberList))
			return false;
		if (message == null) {
			if (other.message != null)
				return false;
		} else if (!message.equals(other.message))
			return false;
		if (urlLink == null) {
			if (other.urlLink != null)
				return false;
		} else if (!urlLink.equals(other.urlLink))
			return false;
		return true;
	}

	
	
	
	

}
