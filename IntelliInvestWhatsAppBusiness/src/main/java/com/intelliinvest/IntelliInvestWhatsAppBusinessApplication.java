package com.intelliinvest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.client.RestTemplate;

import com.intelliinvest.util.IntelliInvestConstant;
import com.twilio.Twilio;

@SpringBootApplication
@ComponentScan({"com.intelliinvest"}) 
@EnableAutoConfiguration
public class IntelliInvestWhatsAppBusinessApplication {

	public static void main(String[] args) {
		SpringApplication.run(IntelliInvestWhatsAppBusinessApplication.class, args);
	}
	
	@Bean
	  public RestTemplate restTemplate(RestTemplateBuilder builder) {
	      return builder.build();
	  }
	
	@Bean
	  public void connectTwilio() {
	       Twilio.init(IntelliInvestConstant.ACCOUNT_SID, IntelliInvestConstant.AUTH_TOKEN);;
	  }

}
